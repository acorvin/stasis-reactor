import React from 'react';

export class ArticleTitleFilter extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onChange(e.target.value);
  }

  render() {
    return (
      <div>
        <label>Title</label><input type="text" onChange={this.handleChange} />
      </div>
    );
  }
}

export default ArticleTitleFilter;

