all: dist public bundle.js adoc

.PHONY: dist public jsx adoc clean
.IGNORE: dist public clean

dist:
	mkdir dist 2>/dev/null

public:
	cp -r public/* dist/

jsx:
	./node_modules/.bin/babel src --out-dir build --presets @babel/preset-react
	cp src/*.css build/

bundle.js: jsx
	./node_modules/.bin/webpack

adoc:
	mkdir -p dist/docs 2>/dev/null
	node hack/process-adoc.js --source docs  --destination dist/docs --templates templates

clean:
	rm -r build dist 2> /dev/null
