// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="what-is-content-repo"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= What is a custom content repository?
//In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
//Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

A custom content repository is a Git repository that you can use to add
AsciiDoc document files that will be processed by Stasis Reactor. You can also
use these repositories to create HTML files that will change the look and feel
of a site.

Stasis Reactor will look for a few special directories when scanning a content
repository:

* `docs`, this directory contains all your AsciiDoc files.
* `public`, this directory contains any HTML files you wish to override and
  also any files you wish to be served by the static site.
* `templates`, this directory contains Nunjucks templates to further customize
  the document conversion process.

These directories map directly to the directories with the same names in the
Stasis Reactor repository. When they are used with a cloud deployment, the
tooling inside Stasis Reactor will copy them into the deployment image. In this
manner you can overwrite default files in the Stasis Reactor repository by
giving the files the same names.

When using a custom content repository with the Stasis Reactor tooling, you
should be aware that all of the default documentation content will be erased
before deploying your documents.

.Additional resources

* link:how-to-add-content-openshift.html[How to add custom content in OpenShift]
* https://git-scm.com[Git documentation]
* https://mozilla.github.io/nunjucks/[Nunjucks documentation]
