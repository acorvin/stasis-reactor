// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: my-concept-module-a.adoc
// * ID: [id="my-concept-module-a-{context}"]
// * Title: = My concept module A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="what-is-document-browser"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= What is the document browser?
//In the title of concept modules, include nouns or noun phrases that are used in the body text. This helps readers and search engines find the information quickly.
//Do not start the title of concept modules with a verb. See also _Wording of headings_ in _The IBM Style Guide_.

The document browser is the heart of the Stasis Reactor. It contains a list
of the documents that exist in the deployment as well as a form to filter
the documents based on their titles and/or their tags.

The title filter is a simple form that will prune the document list based on
matches against the text input to the form. Only documents that contain the
text fragment will be listed.

The tag filter contains a sequence of button cooresponding to content tags in
the documents. By selecting the buttons the document list will be pruned to
match only the documents containing those tags.

.Additional resources

